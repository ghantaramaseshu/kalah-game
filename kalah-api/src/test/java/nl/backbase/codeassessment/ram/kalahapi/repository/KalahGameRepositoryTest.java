package nl.backbase.codeassessment.ram.kalahapi.repository;

import nl.backbase.codeassessment.ram.kalahapi.KalahApiApplication;
import nl.backbase.codeassessment.ram.kalahapi.entity.KalahGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {KalahApiApplication.class, H2JpaConfig.class})
class KalahGameRepositoryTest {

    @Autowired
    private KalahGameRepository kalahGameRepository;

    @BeforeEach
    public void init(){
        kalahGameRepository.deleteAll();
    }

    @Test
    @DisplayName("Given KalahGame when save Fetch All")
    public void givenKalahGameWhenSaveThenFetchAll(){
        //Given
        KalahGame kalahGame = new KalahGame();

        //when
        kalahGameRepository.save(kalahGame);

        //Then
        List<KalahGame> result = StreamSupport
                .stream(kalahGameRepository.findAll().spliterator(),false)
                .collect(Collectors.toList());

        assertEquals(1, result.size());
        assertNotNull(result.get(0).getId());

    }

}