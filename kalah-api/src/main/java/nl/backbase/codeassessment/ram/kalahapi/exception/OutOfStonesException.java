package nl.backbase.codeassessment.ram.kalahapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class OutOfStonesException extends Exception {
    public OutOfStonesException(Integer pitId) {
        super(String.format("No Stones left in the given Pit id:%s Please Select Other Valid Pit",pitId));
    }
}
