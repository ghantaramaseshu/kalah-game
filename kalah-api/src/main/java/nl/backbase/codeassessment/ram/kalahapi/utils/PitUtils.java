package nl.backbase.codeassessment.ram.kalahapi.utils;

import nl.backbase.codeassessment.ram.kalahapi.model.NorthKalahHouse;
import nl.backbase.codeassessment.ram.kalahapi.model.NorthPit;
import nl.backbase.codeassessment.ram.kalahapi.model.Pit;
import nl.backbase.codeassessment.ram.kalahapi.model.PlayerTurn;
import nl.backbase.codeassessment.ram.kalahapi.model.SouthKalahHouse;
import nl.backbase.codeassessment.ram.kalahapi.model.SouthPit;


public class PitUtils {

    public static Boolean isNorthPit(Pit pit){

        return NorthPit.class.isInstance(pit);
    }

    public static Boolean isSouthPit(Pit pit){

        return SouthPit.class.isInstance(pit);
    }

    public static Boolean isNorthKalahaHouse(Pit pit){
        return NorthKalahHouse.class.isInstance(pit);
    }

    public static Boolean isSouthKalahaHouse(Pit pit){
        return SouthKalahHouse.class.isInstance(pit);
    }

    public static Boolean isNotPlayerPit(PlayerTurn playerTurn, Pit pit){
        return !isPlayerPit(playerTurn,pit);
    }

    public static Boolean isPlayerPit(PlayerTurn playerTurn, Pit pit){
        Boolean isPlayerPit = Boolean.FALSE;

        if(playerTurn.equals(PlayerTurn.NORTH)){
            isPlayerPit = isNorthPit(pit);
        }else{
            isPlayerPit = isSouthPit(pit);
        }
        return isPlayerPit;
    }

    public static Boolean isNotPlayerKalahaHouse(Pit pit, PlayerTurn playerTurn){
        return !isPlayerKalahaHouse(pit,playerTurn);
    }

    public static Boolean isPlayerKalahaHouse(Pit pit, PlayerTurn playerTurn){
        Boolean isPlayerKalahaHouse = Boolean.FALSE;
        if(playerTurn.equals(PlayerTurn.NORTH)){
            isPlayerKalahaHouse = isNorthKalahaHouse(pit);
        }else{
            isPlayerKalahaHouse = isSouthKalahaHouse(pit);
        }

        return isPlayerKalahaHouse;
    }

    public static Boolean isKalahaHouse(Pit pit){
        return isNorthKalahaHouse(pit) || isSouthKalahaHouse(pit);
    }


}
