package nl.backbase.codeassessment.ram.kalahapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PitNotFoundException extends Exception {
    public PitNotFoundException(Integer pitId) {
        super(String.format("PitId:%s Not found.",pitId));
    }
}
