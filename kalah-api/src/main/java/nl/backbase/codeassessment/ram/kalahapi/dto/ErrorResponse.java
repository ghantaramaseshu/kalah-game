package nl.backbase.codeassessment.ram.kalahapi.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorResponse {
    private String message;

}
