package nl.backbase.codeassessment.ram.kalahapi.model;


import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *Represents an South Kalah side in which contains List of Pits and a House
 */
public class SouthRow extends KalahRow<SouthPit, SouthKalahHouse> implements Serializable {


    public SouthRow(List<SouthPit> pits, SouthKalahHouse house) {
        super(pits, house);
    }


    public static SouthRowBuilder builder(){
        return new SouthRowBuilder();
    }


    /**
     * SouthRowBuilder implements Builder pattern and build SouthRow.
     */
    public static final class SouthRowBuilder {

        private Integer numberOfPitsInSouth;
        private Integer numberOfStonesPerPit;
        private Integer startPitId = 0;
        private Integer southKalahaHouseId = 0;

        public SouthRowBuilder numberOfPitsInSouth(Integer numberOfPitsInSouth){
            Validate.isTrue( numberOfPitsInSouth > 0 ,
                    "Number of Pits in South must be greater then zero: %d", numberOfPitsInSouth);
            this.numberOfPitsInSouth = numberOfPitsInSouth;
            return this;
        }

        public SouthRowBuilder numberOfStonesPerPit(Integer numberOfStonesPerPit){
            Validate.isTrue( numberOfStonesPerPit > 0 ,
                    "Number of Stones per Pits must be greater then zero: %d", numberOfStonesPerPit);
            this.numberOfStonesPerPit = numberOfStonesPerPit;
            return this;
        }

        public SouthRowBuilder startPitId(Integer startPitId){
            Validate.isTrue( startPitId >= 0 ,
                    "Start Id of Pits in South must be greater then zero: %d", startPitId);
            this.startPitId = startPitId;
            return this;
        }

        public SouthRowBuilder southKalahaHouseId(Integer southKalahaHouseId){
            Validate.isTrue( southKalahaHouseId > 0 ,
                    "South kalaha house Id must be greater then zero: %d", southKalahaHouseId);
            this.southKalahaHouseId = southKalahaHouseId;
            return this;
        }

        public SouthRow build(){

            List<SouthPit> southPits = IntStream.range(getStartPitId(), getEndRangeForPits())
                    .mapToObj(i -> new SouthPit(i, numberOfStonesPerPit))
                    .collect(Collectors.toList());

            updatePitsWithNextPit(southPits);

            SouthKalahHouse house = new SouthKalahHouse(getSouthKalahaHouseId(),0);

            SouthRow southRow = new SouthRow(southPits, house);

            return southRow;
        }

        private Integer getStartPitId(){
            return startPitId;
        }

        private Integer getEndRangeForPits(){
            return startPitId + numberOfPitsInSouth;
        }

        private Integer getSouthKalahaHouseId(){
            return southKalahaHouseId.equals(0) ? getEndRangeForPits() : southKalahaHouseId;
        }

        private void updatePitsWithNextPit(List<SouthPit> southPits){
            IntStream.range(0, southPits.size() -1)
                    .forEach(index ->{
                        southPits.get(index).setNextPit(southPits.get(index + 1));
                    });
        }


    }
}
