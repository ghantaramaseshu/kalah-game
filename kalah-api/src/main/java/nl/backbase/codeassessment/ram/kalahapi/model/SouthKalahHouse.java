package nl.backbase.codeassessment.ram.kalahapi.model;

/**
 * Represents South side of the kalah House
 */
public class SouthKalahHouse extends Pit{

    public SouthKalahHouse(Integer id, Integer stones) {
        super(id, stones);
    }
}
