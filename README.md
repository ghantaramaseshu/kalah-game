# Kalah Game

## Building and Running
This project is written in Java and uses Maven for building.
1. git clone https://bitbucket.org/ghantaramaseshu/kalah-game.git
2. cd kalah-game
3. mvn clean install
4. cd kalah-api
5. mvn spring-boot:run
#Test

## Flow Diagrams
##### Overview
![](overview.png)

##### Sequence
![](sequence.png)
