package integrationtest;


import nl.backbase.codeassessment.ram.kalahapi.KalahApiApplication;
import nl.backbase.codeassessment.ram.kalahapi.dto.GameCreatedResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,classes = KalahApiApplication.class)
@TestPropertySource("classpath:application-test.properties")
public class CreateKalahBoardApiTest {

    @Value("${kalah.board.api.url}")
    private String baseURL;

    @Autowired
    private TestRestTemplate testRestTemplate;

    private static final HttpHeaders headers;

    static {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @Test
    public void testCreateKalahGame() throws URISyntaxException {

        URI uri = new URI(baseURL+"/games");

        HttpEntity request = new HttpEntity(null, headers);

        ResponseEntity<GameCreatedResponse> response = testRestTemplate.postForEntity(uri, request, GameCreatedResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertNotNull(response.getBody().getId());
        assertNotNull(response.getBody().getUri());
    }

}
