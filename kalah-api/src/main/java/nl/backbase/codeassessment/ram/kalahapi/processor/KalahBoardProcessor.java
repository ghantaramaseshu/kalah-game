package nl.backbase.codeassessment.ram.kalahapi.processor;

import lombok.extern.slf4j.Slf4j;
import nl.backbase.codeassessment.ram.kalahapi.entity.SelectedPitEvent;
import nl.backbase.codeassessment.ram.kalahapi.exception.ForbiddenPitAccessException;
import nl.backbase.codeassessment.ram.kalahapi.exception.OutOfStonesException;
import nl.backbase.codeassessment.ram.kalahapi.model.KalahBoard;
import nl.backbase.codeassessment.ram.kalahapi.model.NorthKalahHouse;
import nl.backbase.codeassessment.ram.kalahapi.model.NorthPit;
import nl.backbase.codeassessment.ram.kalahapi.model.Pit;
import nl.backbase.codeassessment.ram.kalahapi.exception.PitNotFoundException;
import nl.backbase.codeassessment.ram.kalahapi.model.PlayerTurn;
import nl.backbase.codeassessment.ram.kalahapi.model.SouthKalahHouse;
import nl.backbase.codeassessment.ram.kalahapi.model.SouthPit;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static nl.backbase.codeassessment.ram.kalahapi.utils.PitUtils.isNorthKalahaHouse;
import static nl.backbase.codeassessment.ram.kalahapi.utils.PitUtils.isNorthPit;
import static nl.backbase.codeassessment.ram.kalahapi.utils.PitUtils.isNotPlayerKalahaHouse;
import static nl.backbase.codeassessment.ram.kalahapi.utils.PitUtils.isNotPlayerPit;
import static nl.backbase.codeassessment.ram.kalahapi.utils.PitUtils.isPlayerPit;
import static nl.backbase.codeassessment.ram.kalahapi.utils.PitUtils.isSouthPit;

/**
 * KalahBoardProcessor Offers services to the kalah Board
 */
@Service
@Slf4j
public class KalahBoardProcessor {


    /**
     * This Method Perform moving of stones from selected pit to corresponding pits
     * @param kalahBoard on which the move to be perform
     * @param pitId id of the Pit to perform pickStones and Spreed those stones to corresponding pits
     * @return kalahBoard status after the move
     * @throws PitNotFoundException given pitId is not found in the KalahBoard
     * @throws ForbiddenPitAccessException if player try to access other side pits or house pits
     * @throws OutOfStonesException if player pick the pit.stones = 0
     * @throws InterruptedException
     */
    public KalahBoard makeMove(KalahBoard kalahBoard, Integer pitId)
            throws PitNotFoundException, ForbiddenPitAccessException, OutOfStonesException, InterruptedException {

        log.info("Started moving stones from pit id:{} for GameId:{}", pitId, kalahBoard.getGameId());
        Pit playerChosenPit = getPitBasedOnPlayerTurn(kalahBoard, pitId);
        try {
            if(kalahBoard.getWriteLock().tryLock(5, TimeUnit.SECONDS)){
                log.debug("Acquired Write Lock on kalahBoard id:{}",kalahBoard.getGameId());
                Pit lastStonePlacedPit = spreadStonesFromPit(playerChosenPit, kalahBoard);
                checkAndCaptureOppositePitStones(lastStonePlacedPit, kalahBoard);
                checkIfGameOverCaptureAllRemainingStones(kalahBoard);
                swapPlayerTurn(lastStonePlacedPit, kalahBoard);
            }
        }finally {
            kalahBoard.getWriteLock().unlock();
            log.debug("Released Write Lock on kalahBoard id:{}",kalahBoard.getGameId());
        }

        return kalahBoard;

    }

    /**
     * @param kalahBoard
     * @param selectedPitEvent
     * @return KalahBoard after performing the event
     * @throws InterruptedException
     * @throws PitNotFoundException
     * @throws ForbiddenPitAccessException
     * @throws OutOfStonesException
     */
    public KalahBoard loadBoard(KalahBoard kalahBoard, SelectedPitEvent selectedPitEvent) throws InterruptedException,
            PitNotFoundException, ForbiddenPitAccessException, OutOfStonesException {
        kalahBoard = makeMove(kalahBoard, selectedPitEvent.getPitId());

        return kalahBoard;
    }

    /**
     * Loads the kalahBoard by performing all the moves based on the List of selected pit events
     * @param kalahBoard
     * @param selectedPitEvents List of selectedPitEvents
     * @return KalahBoard after performing all the events
     * @throws InterruptedException
     * @throws PitNotFoundException
     * @throws ForbiddenPitAccessException
     * @throws OutOfStonesException
     */
    public KalahBoard loadBoard(KalahBoard kalahBoard, List<SelectedPitEvent> selectedPitEvents) throws InterruptedException,
            PitNotFoundException, ForbiddenPitAccessException, OutOfStonesException{

        for (SelectedPitEvent selectedPitEvent : selectedPitEvents) {
            kalahBoard = this.loadBoard(kalahBoard, selectedPitEvent);
        }

        return kalahBoard;

    }

    /**
     * Spread the stones from playerChosenPit to corresponding pits
     * @param playerChosenPit
     * @param kalahBoard
     * @return Pit which is last Stone Placed
     * @throws OutOfStonesException
     */
    private Pit spreadStonesFromPit(Pit playerChosenPit, KalahBoard kalahBoard) throws OutOfStonesException {
        Integer stones = playerChosenPit.pickStones();
        if(stones <= 0){
            log.info("Pit id:{} of KalahBoard id:{} selected by player to move stones has ZERO stones",
                    playerChosenPit.getId(), kalahBoard.getGameId());
            throw new OutOfStonesException(playerChosenPit.getId());
        }
        Pit currentPit=playerChosenPit;
        while(stones > 0){
            currentPit = getNextPit(currentPit, kalahBoard);
            currentPit.addOneStone();
            --stones;
        }
        log.debug("Stones from Pit id:{} of KalahBoard id:{} is successfully spreads to corresponding pits",playerChosenPit.getId(),kalahBoard.getGameId());
        return currentPit;
    }

    /**
     * check if the last Stone Placed Pit is eligible to CaptureOppositePit and capture oppositePit.stones
     * @param lastStonePlacedPit Pit that was last stone placed in SpreadingStones step
     * @param kalahBoard
     */
    private void checkAndCaptureOppositePitStones(Pit lastStonePlacedPit, KalahBoard kalahBoard) {
        if(isPitEligibleToCaptureOppositePit(lastStonePlacedPit, kalahBoard.getPlayerTurn())){
            Pit kalahaHouse = kalahBoard.getKalahaHouseByPlayerTurn();
            Pit oppositePit = getOppositePit(lastStonePlacedPit);
            log.debug("started capturing stones from oppositePit (id :{}) of lastStonePlacedPit (id:{})",
                    oppositePit.getId(),lastStonePlacedPit.getId() );
            Integer capturedStones = lastStonePlacedPit.pickStones() + oppositePit.pickStones();
            kalahaHouse.addStones(capturedStones);
            log.debug("successfully captured stones from oppositePit (id :{}) of lastStonePlacedPit (id:{})",
                    oppositePit.getId(),lastStonePlacedPit.getId() );
        }
    }

    /**
     * check if game is over and capture all remaining stones
     * @param kalahBoard
     */
    private void checkIfGameOverCaptureAllRemainingStones(KalahBoard kalahBoard) {
        if(kalahBoard.isGameOver()){
            log.debug("Game completed on KalahBoard id:{} started capturing remaing stones from other side pits", kalahBoard.getGameId());
            Integer remainingStonesInPits = kalahBoard.pickStonesFromAllPits();
            if(PlayerTurn.NORTH.equals(kalahBoard.getPlayerTurn())){
                kalahBoard.getNorthRow().getHouse().addStones(remainingStonesInPits);
            }else {
                kalahBoard.getSouthRow().getHouse().addStones(remainingStonesInPits);
            }

            kalahBoard.updateWiner();
            log.debug("Successfully completed capturing remaining stones from other side pits on KalahBoard id:{}", kalahBoard.getGameId());
        }
    }

    private void swapPlayerTurn(Pit lastStonePlacedPit, KalahBoard kalahBoard) {
        if(isNotPlayerKalahaHouse(lastStonePlacedPit, kalahBoard.getPlayerTurn())){
            kalahBoard.swapPlayerTurn();
            log.debug("Successfully Player swapped  on KalahBoard id :{} to player :{}", kalahBoard.getGameId(), kalahBoard.getPlayerTurn());
        }
    }

    private Pit getOppositePit(Pit pit) {
        Pit oppositePit;
        if(isNorthPit(pit)){
            NorthPit currentNorthPit = (NorthPit) pit;
            oppositePit = currentNorthPit.getOppositePit();
        }else {
            SouthPit currentSouthPit = (SouthPit) pit;
            oppositePit = currentSouthPit.getOppositePit();
        }
        return oppositePit;
    }

    private boolean isPitEligibleToCaptureOppositePit(Pit pit, PlayerTurn playerTurn) {
        return pit.containsOneStone() && isPlayerPit(playerTurn,pit);
    }



    private Pit getNextPit(Pit currentPit, KalahBoard kalahBoard){
        Pit nextPit ;
        if(isNorthPit(currentPit)){
            NorthPit currentNorthPit = (NorthPit) currentPit;
            nextPit = getNextPit(currentNorthPit, kalahBoard.getPlayerTurn(), kalahBoard.getNorthRow().getHouse());
        }else if(isSouthPit(currentPit)){
            SouthPit currentSouthPit = (SouthPit) currentPit;
            nextPit = getNextPit(currentSouthPit, kalahBoard.getPlayerTurn(), kalahBoard.getSouthRow().getHouse());
        }else if(isNorthKalahaHouse(currentPit)){
            nextPit = kalahBoard.getSouthRow().getFirstPit();
        }else {
            nextPit = kalahBoard.getNorthRow().getFirstPit();
        }

        return nextPit;
    }

    private Pit getNextPit(NorthPit currentNorthPit, PlayerTurn playerTurn,  NorthKalahHouse kalahaHouse){
        Pit nextPit ;
        if(currentNorthPit.hasNextPit()){
            nextPit = currentNorthPit.nextPit();
        }else if(PlayerTurn.NORTH.equals(playerTurn)){
            nextPit = kalahaHouse;
        }else {
            nextPit = currentNorthPit.getOppositePit();
        }

        return nextPit;
    }

    private Pit getNextPit(SouthPit currentSouthPit, PlayerTurn playerTurn,  SouthKalahHouse kalahaHouse){
        Pit nextPit ;
        if(currentSouthPit.hasNextPit()){
            nextPit = currentSouthPit.nextPit();
        }else if(PlayerTurn.SOUTH.equals(playerTurn)){
            nextPit = kalahaHouse;
        }else {
            nextPit = currentSouthPit.getOppositePit();
        }

        return nextPit;
    }


    /**
     * @param kalahBoard
     * @param pitId id of Pit
     * @return Pit if the given pitID is not house or other Side pit
     * @throws PitNotFoundException
     * @throws ForbiddenPitAccessException
     */
    private Pit getPitBasedOnPlayerTurn(KalahBoard kalahBoard, Integer pitId)
            throws PitNotFoundException, ForbiddenPitAccessException {
        Pit pit = kalahBoard.getPitById(pitId);

        if(isNotPlayerPit(kalahBoard.getPlayerTurn(), pit)){
            log.info("Player:%s chose Wrong pit id:{} for gameId:{}",kalahBoard.getPlayerTurn(),
                    pit.getId(),kalahBoard.getGameId());
            throw new ForbiddenPitAccessException(kalahBoard.getPlayerTurn(),pitId);
        }
        log.debug("Successfully Retrieved Pit with id:{} from KalahBoard id:{}", pitId, kalahBoard.getGameId());
        return pit;
    }

}
