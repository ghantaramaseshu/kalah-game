package nl.backbase.codeassessment.ram.kalahapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Setter
@Getter
public class GameCreatedResponse {
    private Long id;
    private String uri;

    @JsonIgnore
    public static GameCreatedResponseBuilder builder(){
        return new GameCreatedResponseBuilder();
    }

    public static class GameCreatedResponseBuilder {
        private Long id;

        public GameCreatedResponseBuilder id(Long id){
            this.id = id;
            return this;
        }

        public GameCreatedResponse build(){
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                    .buildAndExpand(id).toUri();

            GameCreatedResponse gameCreatedResponse = new GameCreatedResponse();
            gameCreatedResponse.setId(id);
            gameCreatedResponse.setUri(location.toString());

            return gameCreatedResponse;
        }

    }
}
