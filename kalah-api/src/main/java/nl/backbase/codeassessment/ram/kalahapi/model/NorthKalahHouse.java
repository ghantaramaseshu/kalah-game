package nl.backbase.codeassessment.ram.kalahapi.model;

/**
 * Represents an North side Kaala house
 */
public class NorthKalahHouse extends Pit{
    public NorthKalahHouse(Integer id, Integer stones) {
        super(id, stones);
    }
}
