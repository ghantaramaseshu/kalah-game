package nl.backbase.codeassessment.ram.kalahapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class KalahBoardNotFoundException extends Exception {
    public KalahBoardNotFoundException(Long id) {
        super(String.format("KalahaBoard Not found with give Id:%s",id));
    }
}
