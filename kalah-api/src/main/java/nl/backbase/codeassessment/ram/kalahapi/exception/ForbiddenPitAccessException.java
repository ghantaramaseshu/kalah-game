package nl.backbase.codeassessment.ram.kalahapi.exception;

import nl.backbase.codeassessment.ram.kalahapi.model.PlayerTurn;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class ForbiddenPitAccessException extends Exception {
    public ForbiddenPitAccessException(PlayerTurn playerTurn, Integer pitId) {
        super(String.format("Player:%s does not have access to Pit Id:%s",playerTurn,pitId));
    }
}
