package nl.backbase.codeassessment.ram.kalahapi.repository;

import nl.backbase.codeassessment.ram.kalahapi.entity.KalahGame;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KalahGameRepository extends CrudRepository<KalahGame, Long> {

}
