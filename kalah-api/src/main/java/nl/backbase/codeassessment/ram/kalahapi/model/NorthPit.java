package nl.backbase.codeassessment.ram.kalahapi.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Represents pits on North side of kalah board
 */
@Getter
@Setter
public class NorthPit extends Pit implements Serializable {

    private NorthPit nextPit;
    private SouthPit oppositePit;


    public NorthPit(Integer id, Integer stones) {
        super(id, stones);
    }


    /**
     * @return true if next NorthPit is available
     */
    public boolean hasNextPit(){
        return nextPit != null;
    }


    /**
     * @return next NorthPit if available else null
     */
    public NorthPit nextPit(){
        return nextPit;
    }

    public void setNextPit(NorthPit nextPit){
        this.nextPit = nextPit;
    }


    /**
     * @return opposite SouthPit of the Board
     */
    public SouthPit getOppositePit() {
        return oppositePit;
    }

    
    public void setOppositePit(SouthPit oppositePit) {
        this.oppositePit = oppositePit;
    }
}
