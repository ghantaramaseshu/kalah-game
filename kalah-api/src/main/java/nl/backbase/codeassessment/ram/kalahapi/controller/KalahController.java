package nl.backbase.codeassessment.ram.kalahapi.controller;


import lombok.extern.slf4j.Slf4j;
import nl.backbase.codeassessment.ram.kalahapi.dto.GameCreatedResponse;
import nl.backbase.codeassessment.ram.kalahapi.dto.KalahGameStatusResponse;
import nl.backbase.codeassessment.ram.kalahapi.exception.ForbiddenPitAccessException;
import nl.backbase.codeassessment.ram.kalahapi.exception.KalahBoardNotFoundException;
import nl.backbase.codeassessment.ram.kalahapi.exception.OutOfStonesException;
import nl.backbase.codeassessment.ram.kalahapi.exception.PitNotFoundException;
import nl.backbase.codeassessment.ram.kalahapi.model.KalahBoard;

import nl.backbase.codeassessment.ram.kalahapi.service.KalahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/games")
@Slf4j
public class KalahController {


    @Autowired
    private KalahService kalahService;

    @PostMapping(value="",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public GameCreatedResponse createGame(){
        log.debug("incoming request for createGame");
        KalahBoard kalahBoard = kalahService.createBoard();

        GameCreatedResponse response = GameCreatedResponse.builder()
                .id(kalahBoard.getGameId())
                .build();

        log.debug("successfully created the kalahBoard id:{}",kalahBoard.getGameId());

        return response;
    }

    @PutMapping(value="/{gameId}/pits/{pitId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public KalahGameStatusResponse makeMove(@PathVariable Long gameId, @PathVariable Integer pitId)
            throws InterruptedException, KalahBoardNotFoundException, PitNotFoundException, ForbiddenPitAccessException,
            OutOfStonesException {

        log.debug("incoming request for makeMove with Params gameId:{} pitId:{}", gameId, pitId);

        KalahBoard kalahBoard = kalahService.fetchAndMakeMove(gameId, pitId);

        Map<Integer, Integer> status = kalahBoard.getPitsMap().entrySet()
                .stream()
                .collect(Collectors.toUnmodifiableMap(e->e.getValue().getId(), e -> e.getValue().getStones()));

        KalahGameStatusResponse kalahGameStatusResponse = KalahGameStatusResponse.builder()
                .id(gameId)
                .status(status)
                .build();

        log.debug("Successfully moved stones from Pit id:{}, on KalahBoard id:{}", pitId, gameId);

        return kalahGameStatusResponse;
    }

    @GetMapping(value = "/{gameId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public KalahGameStatusResponse getGame(@PathVariable Long gameId) throws InterruptedException,
            KalahBoardNotFoundException, PitNotFoundException, ForbiddenPitAccessException, OutOfStonesException {

        log.debug("incoming request for getGame with Params gameId:{}", gameId);

        KalahBoard kalahBoard = kalahService.fetchGame(gameId);

        Map<Integer, Integer> status = kalahBoard.getPitsMap().entrySet()
                .stream()
                .collect(Collectors.toUnmodifiableMap(e->e.getValue().getId(), e -> e.getValue().getStones()));

        KalahGameStatusResponse kalahGameStatusResponse = KalahGameStatusResponse.builder()
                .id(gameId)
                .status(status)
                .build();

        return kalahGameStatusResponse;
    }






}
