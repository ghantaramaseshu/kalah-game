package nl.backbase.codeassessment.ram.kalahapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Map;

@Setter
@Getter
public class KalahGameStatusResponse {
    private Long id;
    private String url;
    private Map<Integer, Integer> status;

    @JsonIgnore
    public static KalahGameStatusResponseBuilder builder(){
        return new KalahGameStatusResponseBuilder();
    }


    public static class KalahGameStatusResponseBuilder{
        private Long id;
        private Map<Integer, Integer> status;

        public KalahGameStatusResponseBuilder id(Long id){
            this.id = id;
            return this;
        }

        public KalahGameStatusResponseBuilder status(Map<Integer, Integer> status){
            this.status = status;
            return this;
        }

        public KalahGameStatusResponse build(){
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                    .buildAndExpand(id).toUri();

            KalahGameStatusResponse kalahGameStatusResponse = new KalahGameStatusResponse();
            kalahGameStatusResponse.setId(id);
            kalahGameStatusResponse.setUrl(location.toString());
            kalahGameStatusResponse.setStatus(status);

            return kalahGameStatusResponse;
        }

    }


}
