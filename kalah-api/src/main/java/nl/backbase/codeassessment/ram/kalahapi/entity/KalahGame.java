package nl.backbase.codeassessment.ram.kalahapi.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "kalah_game")
public class KalahGame {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="game_id", nullable = false)
    @OrderBy("selected_at")
    private List<SelectedPitEvent> selectedPitEvents;
    private boolean gameOver;
    private String winner;

    public KalahGame() {
        gameOver = false;
        selectedPitEvents = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SelectedPitEvent> getPitMoveEvents() {
        return selectedPitEvents;
    }

    public void setPitMoveEvents(List<SelectedPitEvent> selectedPitEvents) {
        this.selectedPitEvents = selectedPitEvents;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public void addSelectedPitEvent(SelectedPitEvent selectedPitEvent){
        Optional.ofNullable(selectedPitEvent).ifPresent(this.selectedPitEvents::add);
    }
}
