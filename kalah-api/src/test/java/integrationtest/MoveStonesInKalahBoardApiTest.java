package integrationtest;

import nl.backbase.codeassessment.ram.kalahapi.KalahApiApplication;
import nl.backbase.codeassessment.ram.kalahapi.dto.ErrorResponse;
import nl.backbase.codeassessment.ram.kalahapi.dto.GameCreatedResponse;
import nl.backbase.codeassessment.ram.kalahapi.dto.KalahGameStatusResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,classes = KalahApiApplication.class)
@TestPropertySource("classpath:application-test.properties")
public class MoveStonesInKalahBoardApiTest {

    @Value("${kalah.board.api.url}")
    private String baseURL;

    @Autowired
    private TestRestTemplate testRestTemplate;

    private Long gameId;

    private static final HttpHeaders headers;

    static {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
    }


    @BeforeEach
    void init() throws URISyntaxException {
        URI uri = new URI(baseURL+"/games");

        HttpEntity request = new HttpEntity(null, headers);

        ResponseEntity<GameCreatedResponse> response = testRestTemplate.postForEntity(uri, request, GameCreatedResponse.class);

        gameId = response.getBody().getId();
    }

    @Test
    public void moveStonesFromInvalidPit() throws URISyntaxException {
        URI uri = new URI(baseURL+"/games/"+gameId+"/pits/20");
        HttpEntity request = new HttpEntity(null, headers);
        ResponseEntity<ErrorResponse> response = testRestTemplate.exchange(uri, HttpMethod.PUT, request, ErrorResponse.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat("PitId:20 Not found.").isEqualTo(response.getBody().getMessage());
    }

    @Test
    public void moveStonesFromInvalidKalahBoard() throws URISyntaxException {
        URI uri = new URI(baseURL+"/games/152017771112222222/pits/1");
        HttpEntity request = new HttpEntity(null, headers);
        ResponseEntity<ErrorResponse> response = testRestTemplate.exchange(uri, HttpMethod.PUT, request, ErrorResponse.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat("KalahaBoard Not found with give Id:152017771112222222").isEqualTo(response.getBody().getMessage());
    }

    @Test
    public void moveStonesFromSouthRowPitWhenNorthPlayerTurn() throws URISyntaxException {
        URI uri = new URI(baseURL+"/games/"+gameId+"/pits/9");
        HttpEntity request = new HttpEntity(null, headers);
        ResponseEntity<ErrorResponse> response = testRestTemplate.exchange(uri, HttpMethod.PUT, request, ErrorResponse.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat("Player:north does not have access to Pit Id:9").isEqualTo(response.getBody().getMessage());
    }


    @Test
    public void moveStonesFromValidPit() throws URISyntaxException {
        URI uri = new URI(baseURL+"/games/"+gameId+"/pits/1");

        HttpEntity request = new HttpEntity(null, headers);

        Map<Integer, Integer> expectedStonesCount = Map.ofEntries(
                Map.entry(1,0), Map.entry(2,7), Map.entry(3,7), Map.entry(4,7),
                Map.entry(5,7), Map.entry(6,7), Map.entry(7,1),
                Map.entry(8,6), Map.entry(9,6), Map.entry(10,6), Map.entry(11,6),
                Map.entry(12,6),Map.entry(13,6), Map.entry(14,0));

        ResponseEntity<KalahGameStatusResponse> response = testRestTemplate.exchange(uri, HttpMethod.PUT,
                request, KalahGameStatusResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertNotNull(response.getBody().getId());
        assertNotNull(response.getBody().getUrl());

        assertPitsStoneCount(expectedStonesCount, response.getBody().getStatus());
    }

    private void assertPitsStoneCount(Map<Integer, Integer> expectedStonesCount, Map<Integer, Integer> actualStonesCount){

        for(int index = 1; index < 15; index++){
            assertEquals(expectedStonesCount.get(index), actualStonesCount.get(index));
        }

    }



}
