package nl.backbase.codeassessment.ram.kalahapi.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.function.Predicate;


/**
 * Represents an Kalah side in which contains List of Pits and a House
 * @param <P> Pit
 * @param <H> Kalah House ie NorthKalahHouse or SouthKalahHouse
 */
@Getter
@Setter
public abstract class KalahRow<P extends Pit,H extends Pit> implements Serializable {

    private List<P> pits;
    private H house;

    private static Predicate<Pit> ZERO_STONES_IN_PIT = pit -> pit.getStones() == 0;

    public KalahRow(List<P> pits, H house) {
        this.pits = pits;
        this.house = house;
    }


    public P getFirstPit(){return pits.get(0);
    }

    /**
     * @return Nubmber of stones present in Kalah house
     */
    public Integer getNumberOfStonesInHouse(){
        return house.getStones();
    }

    /**
     * @return Number of stones present in all pits of the Row
     */
    public Integer getNumberOfStonesInPits(){
        return pits.stream().mapToInt(p -> p.getStones()).sum();
    }

    /**
     * @return Number of stones present in all pits of the Row and set those pit.stones to empty
     */
    public Integer pickStonesFromAllPits(){
        return pits.stream().mapToInt(p -> p.pickStones()).sum();
    }

    /**
     * @return true if all pits.stones in row is zero.
     */
    public Boolean isAllPitsHasZeroStones(){
        return pits.stream().allMatch(ZERO_STONES_IN_PIT);
    }

}
