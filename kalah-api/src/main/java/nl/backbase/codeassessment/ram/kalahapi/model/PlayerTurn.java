package nl.backbase.codeassessment.ram.kalahapi.model;

/**
 * Represents Player Turn
 */
public enum PlayerTurn {
    NORTH("north"),
    SOUTH("south");

    private String turn;

    PlayerTurn(String turn){
        this.turn = turn;
    }


    @Override
    public String toString() {
        return turn;
    }
}
