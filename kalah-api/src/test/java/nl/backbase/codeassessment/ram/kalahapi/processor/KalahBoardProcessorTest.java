package nl.backbase.codeassessment.ram.kalahapi.processor;

import nl.backbase.codeassessment.ram.kalahapi.exception.ForbiddenPitAccessException;
import nl.backbase.codeassessment.ram.kalahapi.exception.OutOfStonesException;
import nl.backbase.codeassessment.ram.kalahapi.model.KalahBoard;
import nl.backbase.codeassessment.ram.kalahapi.model.Pit;
import nl.backbase.codeassessment.ram.kalahapi.exception.PitNotFoundException;
import nl.backbase.codeassessment.ram.kalahapi.model.PlayerTurn;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class KalahBoardProcessorTest {

    private KalahBoard subject;
    private KalahBoardProcessor service;

    @BeforeEach
    void init(){
        subject = KalahBoard.builder()
                .numberOfPitsOnEachSide(6)
                .numberOfStonesPerPit(6)
                .playerTurn(PlayerTurn.NORTH)
                .build();
        service = new KalahBoardProcessor();
    }

    @Test
    @DisplayName("test makeMove when NorthPlayer selects southRow Pit then ForbiddenPitAccessException")
    public void testNortPlayerSelectsSouthRowPit(){
        assertThrows(ForbiddenPitAccessException.class, ()->{
            subject = service.makeMove(subject, 10);
        });
    }

    @Test
    @DisplayName("test makeMove when NorthPlayer selects NorthKalahHouse Pit expects ForbiddenPitAccessException")
    public void testNortPlayerSelectsNorthKalahaHousePit() {

        assertThrows(ForbiddenPitAccessException.class, ()->{
            subject = service.makeMove(subject, 7);
        });
    }

    @Test
    @DisplayName("test makeMove when NorthPlayer selects SouthKalahHouse Pit expects ForbiddenPitAccessException")
    public void testNortPlayerSelectsSouthKalahaHousePit() {

        assertThrows(ForbiddenPitAccessException.class, ()->{
            subject = service.makeMove(subject, 14);
        });
    }

    @Test
    @DisplayName("test makeMove when Player North selects Zero stones pit to move then OutOfStonesException")
    public void testMakeMovePlayerSelectsZeroStonesPit() throws InterruptedException, PitNotFoundException,
            ForbiddenPitAccessException, OutOfStonesException {
        subject = service.makeMove(subject, 1);

        assertThrows(OutOfStonesException.class, ()->{
            subject = service.makeMove(subject, 1);
        });
    }

    @Test
    @DisplayName("Test makeMove when last stone of picked stones from the pit landed in KalahaHouse")
    public void testMakeMoveEndStoneLandInKalaHouseByNorthPlayer() throws InterruptedException, PitNotFoundException,
            ForbiddenPitAccessException, OutOfStonesException {

        Map<Integer, Integer> expectedStonesCount = Map.ofEntries(
                Map.entry(1,6), Map.entry(2,6), Map.entry(3,6), Map.entry(4,6),
                Map.entry(5,6), Map.entry(6,6), Map.entry(7,0),
                Map.entry(8,6), Map.entry(9,6), Map.entry(10,6), Map.entry(11,6),
                Map.entry(12,6),Map.entry(13,6), Map.entry(14,0));

        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());

        subject = service.makeMove(subject, 1);

        expectedStonesCount = Map.ofEntries(
                Map.entry(1,0), Map.entry(2,7), Map.entry(3,7), Map.entry(4,7),
                Map.entry(5,7), Map.entry(6,7), Map.entry(7,1),
                Map.entry(8,6), Map.entry(9,6), Map.entry(10,6), Map.entry(11,6),
                Map.entry(12,6),Map.entry(13,6), Map.entry(14,0));

        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());

        assertTrue(PlayerTurn.NORTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

    }

    @Test
    @DisplayName("Test makeMove swap player Turns")
    public void testMakeMoveSwapPlayerTurns() throws InterruptedException, PitNotFoundException,
            ForbiddenPitAccessException, OutOfStonesException {

        subject = service.makeMove(subject, 2);

        Map<Integer, Integer> expectedStonesCount = Map.ofEntries(
                Map.entry(1,6), Map.entry(2,0), Map.entry(3,7), Map.entry(4,7),
                Map.entry(5,7), Map.entry(6,7), Map.entry(7,1),
                Map.entry(8,7), Map.entry(9,6), Map.entry(10,6), Map.entry(11,6),
                Map.entry(12,6),Map.entry(13,6), Map.entry(14,0));

        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());

        assertTrue(PlayerTurn.SOUTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());
    }

    @Test
    @DisplayName("test makeMove with Player South")
    public void testMakeMoveWithPlayerSouth() throws InterruptedException, PitNotFoundException, ForbiddenPitAccessException, OutOfStonesException {
        subject = service.makeMove(subject, 2);
        assertTrue(PlayerTurn.SOUTH.equals(subject.getPlayerTurn()));

        subject = service.makeMove(subject, 8);

        Map<Integer, Integer> expectedStonesCount = Map.ofEntries(
                Map.entry(1,7), Map.entry(2,0), Map.entry(3,7), Map.entry(4,7),
                Map.entry(5,7), Map.entry(6,7), Map.entry(7,1),
                Map.entry(8,0), Map.entry(9,7), Map.entry(10,7), Map.entry(11,7),
                Map.entry(12,7),Map.entry(13,7), Map.entry(14,1));

        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());

        assertTrue(PlayerTurn.NORTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

    }

    @Test
    @DisplayName("Test makeMove CollectOppositePitStones")
    public void testMakeMoveCollectOppositePitStones() throws InterruptedException, PitNotFoundException,
            ForbiddenPitAccessException, OutOfStonesException {

        subject = service.makeMove(subject, 2);

        Map<Integer, Integer> expectedStonesCount = Map.ofEntries(
                Map.entry(1,6), Map.entry(2,0), Map.entry(3,7), Map.entry(4,7),
                Map.entry(5,7), Map.entry(6,7), Map.entry(7,1),
                Map.entry(8,7), Map.entry(9,6), Map.entry(10,6), Map.entry(11,6),
                Map.entry(12,6),Map.entry(13,6), Map.entry(14,0));

        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());
        assertTrue(PlayerTurn.SOUTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

        subject = service.makeMove(subject, 9);
        expectedStonesCount = Map.ofEntries(
                Map.entry(1,7), Map.entry(2,0), Map.entry(3,7), Map.entry(4,7),
                Map.entry(5,7), Map.entry(6,7), Map.entry(7,1),
                Map.entry(8,7), Map.entry(9,0), Map.entry(10,7), Map.entry(11,7),
                Map.entry(12,7),Map.entry(13,7), Map.entry(14,1));
        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());
        assertTrue(PlayerTurn.NORTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

        subject = service.makeMove(subject, 6);
        expectedStonesCount = Map.ofEntries(
                Map.entry(1,7), Map.entry(2,0), Map.entry(3,7), Map.entry(4,7),
                Map.entry(5,7), Map.entry(6,0), Map.entry(7,2),
                Map.entry(8,8), Map.entry(9,1), Map.entry(10,8), Map.entry(11,8),
                Map.entry(12,8),Map.entry(13,8), Map.entry(14,1));
        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());
        assertTrue(PlayerTurn.SOUTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

        subject = service.makeMove(subject, 13);
        expectedStonesCount = Map.ofEntries(
                Map.entry(1,8), Map.entry(2,1), Map.entry(3,8), Map.entry(4,8),
                Map.entry(5,8), Map.entry(6,1), Map.entry(7,2),
                Map.entry(8,9), Map.entry(9,1), Map.entry(10,8), Map.entry(11,8),
                Map.entry(12,8),Map.entry(13,0), Map.entry(14,2));
        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());
        assertTrue(PlayerTurn.NORTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

        subject = service.makeMove(subject, 3);
        expectedStonesCount = Map.ofEntries(
                Map.entry(1,8), Map.entry(2,1), Map.entry(3,0), Map.entry(4,9),
                Map.entry(5,9), Map.entry(6,2), Map.entry(7,3),
                Map.entry(8,10), Map.entry(9,2), Map.entry(10,9), Map.entry(11,9),
                Map.entry(12,8),Map.entry(13,0), Map.entry(14,2));
        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());
        assertTrue(PlayerTurn.SOUTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

        subject = service.makeMove(subject, 9);
        expectedStonesCount = Map.ofEntries(
                Map.entry(1,8), Map.entry(2,1), Map.entry(3,0), Map.entry(4,9),
                Map.entry(5,9), Map.entry(6,2), Map.entry(7,3),
                Map.entry(8,10), Map.entry(9,0), Map.entry(10,10), Map.entry(11,10),
                Map.entry(12,8),Map.entry(13,0), Map.entry(14,2));
        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());
        assertTrue(PlayerTurn.NORTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

        subject = service.makeMove(subject, 2);
        expectedStonesCount = Map.ofEntries(
                Map.entry(1,8), Map.entry(2,0), Map.entry(3,0), Map.entry(4,9),
                Map.entry(5,9), Map.entry(6,2), Map.entry(7,14),
                Map.entry(8,10), Map.entry(9,0), Map.entry(10,10), Map.entry(11,0),
                Map.entry(12,8),Map.entry(13,0), Map.entry(14,2));
        assertPitsStoneCount(expectedStonesCount, subject.getPitsMap());
        assertTrue(PlayerTurn.SOUTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());


    }

    private void assertPitsStoneCount(Map<Integer, Integer> expectedStonesCount, Map<Integer, Pit> actualPitsMap){

        for(int index = 1; index < 15; index++){
            assertEquals(expectedStonesCount.get(index), actualPitsMap.get(index).getStones());
        }

    }



}