package nl.backbase.codeassessment.ram.kalahapi.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents pits on South side of kalah board
 */
@Getter
@Setter
public class SouthPit extends Pit{

    private SouthPit nextPit;

    private NorthPit oppositePit;

    public SouthPit(Integer id, Integer stones) {
        super(id, stones);
    }

    /**
     * @return true if next SouthPit is available else false
     */
    public boolean hasNextPit(){
        return nextPit != null;
    }

    /**
     * @return next SouthPit if available else null
     */
    public SouthPit nextPit(){
        return nextPit;
    }

    public void setNextPit(SouthPit nextPit){
        this.nextPit = nextPit;
    }

    /**
     * @return opposite NorthPit of the Board
     */
    public NorthPit getOppositePit() {
        return oppositePit;
    }

    public void setOppositePit(NorthPit oppositePit) {
        this.oppositePit = oppositePit;
    }
}
