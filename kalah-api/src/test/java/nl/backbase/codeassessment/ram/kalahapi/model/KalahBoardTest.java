package nl.backbase.codeassessment.ram.kalahapi.model;

import nl.backbase.codeassessment.ram.kalahapi.exception.PitNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;

class KalahBoardTest {

    private KalahBoard subject;

    @BeforeEach
    void init(){
        subject = KalahBoard.builder()
                .numberOfPitsOnEachSide(6)
                .numberOfStonesPerPit(6)
                .playerTurn(PlayerTurn.NORTH)
                .build();
    }

    @Test
    @DisplayName("A New KalahaBoard should have correct Pits and houses")
    void testKalahaBoardAttributes(){
        assertNotNull(subject);
        assertNotNull(subject.getNorthRow());
        assertNotNull(subject.getSouthRow());
        assertNotNull(subject.getPitsMap());
        assertTrue(PlayerTurn.NORTH.equals(subject.getPlayerTurn()));
        assertFalse(subject.isGameOver());

    }

    @Test
    @DisplayName("North Row should have correct Pits and house")
    void testPitsAndHouseInNorthRow(){
        assertEquals(0, subject.getNorthRow().getNumberOfStonesInHouse());
        assertEquals(6, subject.getNorthRow().getPits().size());
        assertEquals(36, subject.getNorthRow().getNumberOfStonesInPits());

        for(int index = 0 ; index < 6; index++){
            NorthPit actualPit = subject.getNorthRow().getPits().get(index);
            NorthPit expectedPit = newNorthPit(index+1,6);
            if(index < 5){
                expectedPit.setNextPit(newNorthPit(index+2,6));
            }
            expectedPit.setOppositePit(newSouthPit(13-index, 6));
            assertNorthPitWithOppositeSouthPit(expectedPit, actualPit);
        }
    }

    @Test
    @DisplayName("South Row should have correct Pits and house")
    void testPitsAndHouseInSouthRow(){
        assertEquals(0, subject.getSouthRow().getNumberOfStonesInHouse());
        assertEquals(6, subject.getSouthRow().getPits().size());
        assertEquals(36, subject.getSouthRow().getNumberOfStonesInPits());

        Map<Integer, Integer> expectedMappingNorthIds = Map.ofEntries(entry(8,6), entry(9,5),
                entry(10,4), entry(11,3), entry(12,2), entry(13,1));

        for(int index = 0; index < 6; index++){
            SouthPit actualPit = subject.getSouthRow().getPits().get(index);
            SouthPit expectedPit = newSouthPit(index + 8,6);
            if(index < 5){
                expectedPit.setNextPit(newSouthPit(index + 1 + 8, 6));
            }
            expectedPit.setOppositePit(newNorthPit(expectedMappingNorthIds.get(actualPit.getId()),6));
            assertSouthPitWithOppositeNorthPit(expectedPit, actualPit);
        }
    }

    @Test
    @DisplayName("Test getPitById")
    void testGetPitById() throws PitNotFoundException {
        assertNotNull(subject.getPitById(1));

        assertThrows(PitNotFoundException.class, ()->{
            subject.getPitById(15);
        });
    }

    private void assertNorthPitWithOppositeSouthPit(NorthPit expectedPit, NorthPit actualPit){
        assertNorthPit(expectedPit, actualPit);
        assertSouthPit(expectedPit.getOppositePit(), actualPit.getOppositePit());
    }

    private void assertNorthPit(NorthPit expectedPit, NorthPit actualPit){
        assertPit(expectedPit, actualPit);
        if(expectedPit.hasNextPit()){
            assertTrue(actualPit.hasNextPit());
            assertPit(expectedPit.nextPit(), actualPit.nextPit());
        }

    }

    private void assertSouthPitWithOppositeNorthPit(SouthPit expectedPit, SouthPit actualPit){
        assertSouthPit(expectedPit, actualPit);
        assertNorthPit(expectedPit.getOppositePit(), actualPit.getOppositePit());
    }

    private void assertSouthPit(SouthPit expectedPit, SouthPit actualPit){
        assertPit(expectedPit, actualPit);
        if(expectedPit.hasNextPit()){
            assertTrue(actualPit.hasNextPit());
            assertPit(expectedPit.nextPit(), actualPit.nextPit());
        }
    }

    private void assertPit(Pit expectedPit, Pit actualPit){
        assertEquals(expectedPit.getId() , actualPit.getId());
        assertEquals(expectedPit.getStones() , actualPit.getStones());
    }


    private NorthPit newNorthPit(Integer id, Integer stones){
        NorthPit northPit = new NorthPit(id, stones);
        return northPit;
    }

    private SouthPit newSouthPit(Integer id, Integer stones){
        SouthPit southPit = new SouthPit(id, stones);
        return southPit;
    }

}