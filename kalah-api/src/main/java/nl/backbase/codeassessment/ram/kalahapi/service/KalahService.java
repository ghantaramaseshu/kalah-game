package nl.backbase.codeassessment.ram.kalahapi.service;

import lombok.extern.slf4j.Slf4j;
import nl.backbase.codeassessment.ram.kalahapi.entity.KalahGame;
import nl.backbase.codeassessment.ram.kalahapi.entity.SelectedPitEvent;
import nl.backbase.codeassessment.ram.kalahapi.exception.ForbiddenPitAccessException;
import nl.backbase.codeassessment.ram.kalahapi.exception.KalahBoardNotFoundException;
import nl.backbase.codeassessment.ram.kalahapi.exception.OutOfStonesException;
import nl.backbase.codeassessment.ram.kalahapi.exception.PitNotFoundException;
import nl.backbase.codeassessment.ram.kalahapi.model.KalahBoard;
import nl.backbase.codeassessment.ram.kalahapi.model.PlayerTurn;
import nl.backbase.codeassessment.ram.kalahapi.processor.KalahBoardProcessor;
import nl.backbase.codeassessment.ram.kalahapi.repository.KalahGameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * KalahService will create new KalahBoards and manage them
 */
@Service
@Slf4j
public class KalahService {


    @Value("${kalah.stones.number.pit}")
    private Integer numberOfStonesPerPit;

    @Value("${kalah.pits.number.eachSide}")
    private Integer numberOfPitsOnEachSide;

    @Value("#{'${kalah.player.turn}'.toUpperCase()}")
    private PlayerTurn playerTurn;

    @Autowired
    private KalahGameRepository kalahGameRepository;

    @Autowired
    private KalahBoardProcessor kalahBoardProcessor;

    private static Map<Long, KalahBoard> BOARDS_CACHE =  new ConcurrentHashMap<>();


    /**
     * create new kalahGame and save it to repository
     * @return KalahBoard
     */
    public KalahBoard createBoard(){

        KalahGame kalahGame = new KalahGame();
        kalahGame = kalahGameRepository.save(kalahGame);

        KalahBoard kalahBoard = createNewKalahBoard(kalahGame.getId());

        BOARDS_CACHE.put(kalahBoard.getGameId(), kalahBoard);
        log.debug("new KalahBoar id:{} is created", kalahGame.getId());

        return kalahBoard;
    }


    /**
     * Fetch the kalahBoard from the cache if not will build the kalahBoard from the kalahGame.PitMoveEvents.
     * @param gameId
     * @return KalahBoard
     * @throws KalahBoardNotFoundException
     * @throws InterruptedException
     * @throws PitNotFoundException
     * @throws ForbiddenPitAccessException
     * @throws OutOfStonesException
     */
    public KalahBoard fetchGame(Long gameId) throws KalahBoardNotFoundException, InterruptedException, PitNotFoundException,
            ForbiddenPitAccessException, OutOfStonesException {

        KalahBoard kalahBoard = BOARDS_CACHE.get(gameId);


        if(kalahBoard == null){
            KalahGame kalahGame = fetchKalaGame(gameId);
            kalahBoard = createNewKalahBoard(kalahGame.getId());
            kalahBoard = kalahBoardProcessor.loadBoard(kalahBoard, kalahGame.getPitMoveEvents());
            BOARDS_CACHE.put(kalahBoard.getGameId(), kalahBoard);
        }
        log.debug("Successfully fetched kalahBoard id:{}", gameId);

        return kalahBoard;
    }

    /**
     * fetch the kalahBoard and perform the move of stones from selected PitId
     * this method internally saves this move event in repository
     * @param gameId
     * @param pitId
     * @return KalahBoard after perform the move of stones
     * @throws KalahBoardNotFoundException
     * @throws InterruptedException
     * @throws PitNotFoundException
     * @throws ForbiddenPitAccessException
     * @throws OutOfStonesException
     */
    public KalahBoard fetchAndMakeMove(Long gameId, Integer pitId)throws KalahBoardNotFoundException,
            InterruptedException, PitNotFoundException, ForbiddenPitAccessException, OutOfStonesException {

        KalahBoard kalahBoard = fetchGame(gameId);
        kalahBoard = kalahBoardProcessor.makeMove(kalahBoard, pitId);
        BOARDS_CACHE.put(kalahBoard.getGameId(), kalahBoard);
        recordSelectedPitEvent(gameId, pitId);

        return kalahBoard;
    }


    private SelectedPitEvent recordSelectedPitEvent(Long gameId, Integer pitId) throws KalahBoardNotFoundException {
        KalahGame kalahGame = fetchKalaGame(gameId);
        SelectedPitEvent selectedPitEvent = createNewSelectedPitEvent(pitId);
        kalahGame.addSelectedPitEvent(selectedPitEvent);

        kalahGameRepository.save(kalahGame);
        log.debug("Successfully saved the selectedPitEvent on KalahGame id:{} with pit id:{}", gameId, pitId);

        return selectedPitEvent;
    }

    private KalahGame fetchKalaGame(Long gameId) throws KalahBoardNotFoundException {

        Optional<KalahGame> kalahGameOptional = kalahGameRepository.findById(gameId);

        if(!kalahGameOptional.isPresent()){
            throw new KalahBoardNotFoundException(gameId);
        }

        return kalahGameOptional.get();
    }

    private KalahBoard createNewKalahBoard(Long gameId){
        return KalahBoard.builder()
                .gameId(gameId)
                .numberOfPitsOnEachSide(numberOfPitsOnEachSide)
                .numberOfStonesPerPit(numberOfStonesPerPit)
                .playerTurn(playerTurn)
                .build();
    }

    private SelectedPitEvent createNewSelectedPitEvent(Integer pitId){
        SelectedPitEvent selectedPitEvent = new SelectedPitEvent();
        selectedPitEvent.setPitId(pitId);

        return selectedPitEvent;

    }

}
