package nl.backbase.codeassessment.ram.kalahapi.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Represents Pit in kalah board
 */
@Data
public class Pit implements Serializable {
    private Integer id;
    private Integer stones;


    public Pit(Integer id, Integer stones) {
        this.id = id;
        this.stones = stones;
    }


    public Boolean isEmpty (){
        return this.stones == 0;
    }


    /**
     * Add the one stone to the available stones
     */
    public void addOneStone(){
        this.stones++;
    }

    /**
     * @return number of stones present in this pit and set this pit.stones to zero
     */
    public Integer pickStones(){
        Integer pickedStones = this.stones;
        this.stones = 0;
        return pickedStones;
    }


    /**
     * @param pickedUpStones to be added to this pit available stones
     */
    public void addStones(Integer pickedUpStones){
        this.stones += pickedUpStones;
    }


    /**
     * @return true if this pit contains one stone else false
     */
    public Boolean containsOneStone(){
        return stones == 1;
    }

}
