package nl.backbase.codeassessment.ram.kalahapi.model;


import lombok.Builder;
import lombok.Getter;
import nl.backbase.codeassessment.ram.kalahapi.exception.PitNotFoundException;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Represents an Kalah Board
 */
@Builder
@Getter
public class KalahBoard implements Serializable {

    private Long gameId;
    private SouthRow southRow;
    private NorthRow northRow;
    private PlayerTurn playerTurn;
    private Map<Integer, Pit> pitsMap;
    private Boolean gameOver = Boolean.FALSE;
    private String winner;


    private transient final ReadWriteLock lock =  new ReentrantReadWriteLock();

    private transient final Lock writeLock = lock.writeLock();

    public KalahBoard(Long gameId, SouthRow southRow, NorthRow northRow, PlayerTurn playerTurn) {
        this.gameId = gameId;
        this.southRow = southRow;
        this.northRow = northRow;
        this.playerTurn = playerTurn;
        updatePitsWithOppositePit();
    }

    private void updatePitsWithOppositePit(){
        Stack<NorthPit> northPits = northRow.getPits().stream().collect(Collectors.toCollection(Stack::new));
        Stack<SouthPit> southPits = southRow.getPits().stream().collect(Collectors.toCollection(Stack::new));

        IntStream.range(0, southRow.getPits().size()).forEach(index->{
            southRow.getPits().get(index).setOppositePit(northPits.pop());
            northRow.getPits().get(index).setOppositePit(southPits.pop());
        });
    }


    /**
     * @param pitId an Integer representing the Pit's id
     * @return a Pit in the kalah board
     * @throws PitNotFoundException
     */
    public Pit getPitById(Integer pitId) throws PitNotFoundException {
        if(pitsMap.containsKey(pitId)){
            return pitsMap.get(pitId);
        }else
            throw new PitNotFoundException(pitId);
    }


    /**
     * @return If PlayerTurn is NORTH then will return NorthKalahHouse else SouthKalahHouse
     */
    public Pit getKalahaHouseByPlayerTurn(){
        Pit kalahaHouse;
        if(PlayerTurn.NORTH.equals(playerTurn)){
            kalahaHouse = northRow.getHouse();
        }else{
            kalahaHouse = southRow.getHouse();
        }
        return kalahaHouse;
    }


    /**
     * @return true if stones in all pits of NorthRow or SouthRow is Empty, else false.
     */
    public Boolean isGameOver(){
        gameOver = northRow.isAllPitsHasZeroStones() || southRow.isAllPitsHasZeroStones() ;
        return gameOver;
    }

    /**
     * @return number of Stones available in pits of NorthRow and SouthRow and empty those Pits
     */
    public Integer pickStonesFromAllPits(){
        Integer remainingStonesInNorthPits = northRow.pickStonesFromAllPits();
        Integer remainingStonesInSouthPits = southRow.pickStonesFromAllPits();

        return Integer.sum(remainingStonesInNorthPits, remainingStonesInSouthPits);
    }

    public void updateWiner(){
        if(northRow.getNumberOfStonesInHouse() > southRow.getNumberOfStonesInHouse()){
            winner = PlayerTurn.NORTH.toString();
        }else if(southRow.getNumberOfStonesInHouse() > northRow.getNumberOfStonesInHouse()){
            winner = PlayerTurn.SOUTH.toString();
        }else{
            winner = "draw";
        }

    }

    /**
     * Swap the player turn in board
     */
    public void swapPlayerTurn(){
        if(PlayerTurn.NORTH.equals(playerTurn)){
            playerTurn = PlayerTurn.SOUTH;
        }else{
            playerTurn = PlayerTurn.NORTH;
        }
    }


    /**
     * KalahBoardBuilder with support builder pattern is a helper class to build the kalahBoard.
     */
    public static class KalahBoardBuilder{

        private Long gameId;
        private Integer numberOfPitsOnEachSide;
        private Integer numberOfStonesPerPit;
        private PlayerTurn playerTurn;

        public KalahBoardBuilder gameId(Long gameId){
            this.gameId = gameId;
            return this;
        }
        public KalahBoardBuilder numberOfPitsOnEachSide(Integer numberOfPits){
            Validate.isTrue( numberOfPits > 0 ,
                    "Number of Pits in KalahaBoard must be greater then zero: %d", numberOfPits);
            this.numberOfPitsOnEachSide = numberOfPits;
            return this;
        }

        public KalahBoardBuilder numberOfStonesPerPit(Integer numberOfStonesPerPit){
            Validate.isTrue( numberOfStonesPerPit > 0 ,
                    "Number of Stones per Pits must be greater then zero: %d", numberOfStonesPerPit);
            this.numberOfStonesPerPit = numberOfStonesPerPit;
            return this;
        }

        public KalahBoardBuilder playerTurn(PlayerTurn playerTurn){
            Validate.notNull( playerTurn,"PlayerTurn should not be null while creating KalahBoard");
            this.playerTurn = playerTurn;
            return this;
        }


        public KalahBoard build(){
            Integer nextStartPitId = 1;

            NorthRow northRow = NorthRow.builder()
                    .startPitId(nextStartPitId)
                    .numberOfPitsInNorth(numberOfPitsOnEachSide)
                    .numberOfStonesPerPit(numberOfStonesPerPit)
                    .build();

            nextStartPitId += 1 + numberOfStonesPerPit;

            SouthRow southRow = SouthRow.builder()
                    .startPitId(nextStartPitId)
                    .numberOfPitsInSouth(numberOfPitsOnEachSide)
                    .numberOfStonesPerPit(numberOfStonesPerPit)
                    .build();

            KalahBoard kalahBoard = new KalahBoard(gameId, southRow, northRow, playerTurn);
            kalahBoard.pitsMap  = constructPitsMap(northRow,southRow);

            return kalahBoard;
        }

        private Map<Integer, Pit> constructPitsMap(NorthRow northRow, SouthRow southRow ){
            List<Pit> pitList = new ArrayList<>();

            pitList.addAll(northRow.getPits());
            pitList.add(northRow.getHouse());

            pitList.addAll(southRow.getPits());
            pitList.add(southRow.getHouse());

            return pitList.stream().collect(
                    Collectors.toUnmodifiableMap(pit -> pit.getId(), pit-> pit));
        }

    }


}
