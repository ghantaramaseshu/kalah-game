package nl.backbase.codeassessment.ram.kalahapi.model;

import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 *Represents an North Kalah side in which contains List of Pits and a House
 */
public class NorthRow extends KalahRow<NorthPit, NorthKalahHouse> {


    public NorthRow(List<NorthPit> pits, NorthKalahHouse house) {
        super(pits, house);
    }

    public static NorthRowBuilder builder(){
        return  new NorthRowBuilder();
    }


    /**
     * NorthRowBuilder implements Builder pattern and build NorthRow.
     */
    public static final class NorthRowBuilder {

        private Integer numberOfPitsInNorth;
        private Integer numberOfStonesPerPit;
        private Integer startPitId = 0;
        private Integer northKalahaHouseId = 0;

        public NorthRowBuilder numberOfPitsInNorth(Integer numberOfPitsInNorth){
            Validate.isTrue( numberOfPitsInNorth > 0 ,
                    "Number of Pits in North must be greater then zero: %d", numberOfPitsInNorth);
            this.numberOfPitsInNorth = numberOfPitsInNorth;
            return this;
        }

        public NorthRowBuilder numberOfStonesPerPit(Integer numberOfStonesPerPit){
            Validate.isTrue( numberOfStonesPerPit > 0 ,
                    "Number of Stones per Pits must be greater then zero: %d", numberOfStonesPerPit);
            this.numberOfStonesPerPit = numberOfStonesPerPit;
            return this;
        }

        public NorthRowBuilder startPitId(Integer startPitId){
            Validate.isTrue( startPitId >= 0 ,
                    "Start Id of Pits in North must be greater then zero: %d", startPitId);
            this.startPitId = startPitId;

            return this;
        }

        public NorthRowBuilder northKalahaHouseId(Integer northKalahaHouseId){
            Validate.isTrue( northKalahaHouseId > 0 ,
                    "North kalaha house Id must be greater then zero: %d", northKalahaHouseId);
            this.northKalahaHouseId = northKalahaHouseId;

            return this;
        }

        public NorthRow build(){

            List<NorthPit> northPits = IntStream.range(getStartPitId(), getEndRangeForPits())
                    .mapToObj(i -> new NorthPit(i, numberOfStonesPerPit))
                    .collect(Collectors.toList());

            updatePitsWithNextPit(northPits);

            NorthKalahHouse house = new NorthKalahHouse(getNorthKalahaHouseId(),0);

            NorthRow northRow = new NorthRow(northPits, house);

            return northRow;
        }

        private Integer getStartPitId(){
            return startPitId;
        }

        private Integer getEndRangeForPits(){
            return startPitId + numberOfPitsInNorth;
        }

        private Integer getNorthKalahaHouseId(){
            return northKalahaHouseId.equals(0) ? getEndRangeForPits() : northKalahaHouseId;
        }

        private void updatePitsWithNextPit(List<NorthPit> northPits){
            IntStream.range(0, northPits.size() -1)
                    .forEach(index ->{
                        northPits.get(index).setNextPit(northPits.get(index + 1));
                    });
        }


    }
}
